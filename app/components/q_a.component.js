/**
 * @name VFS AngularJS Questions and Answers Controller
 *
 * @copyright (C) 2014-2017 Kibble Games Inc in cooperation with Vancouver Film School.  All Rights Reserved.
 * @author Aécio Levy
 *
 */

const testData = {
    answer: 'Yes, It is the best city in Western Canada.',
    question: 'Is Vancouver the best city in Western Canada?'
}

export class QeAComponentController {
    constructor() {
        this.title = 'Questions and Answers';
        this.answer = testData.answer;
        this.question = testData.question;
    }
}

angular.module('app.components')
.component('questions', {
    templateUrl: 'app/components/q_a.component.html',
    controller: [QeAComponentController]
});
