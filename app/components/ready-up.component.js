/**
 * @name VFS AngularJS App Component
 *
 * @copyright (C) 2018 Kibble Games Inc in cooperation with Vancouver Film School.  All Rights Reserved.
 * @author Scott Henshaw
 *
 */
'use_strict';

//import { SomeService } from './components/some.service.js';

export class ReadyUpComponentController {

	constructor( /* someServiceINeed */ ) {

		// this.svcProvider = someServiceINeed;
		this.vm = {
			nickname: "dummy"
		};
	}

	ready( nickname ) {

        this.vm.nickname = nickname;
	}
}


angular.module('app.components')
	.component('pgReady',  {
        //templateUrl: 'app/components/sample.html',
        template: `
        <div id="ready-up">
            <form id="nickname" ng-submit="$ctrl.ready( user.nickname )" ng-model="user">
                <input name="nickname-field" type="text" ng-model="user.nickname" />
                <input type="submit" ng-click="$ctrl.ready( user.nickname )" value="Ready!" />
            </form>
        </div>
        `,
        controller:  ReadyUpComponentController,
        bindings:    {
            title:    "@",
        }
    });
